# Terrarm | Déploiement dynamique avec terraform

_______

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com

><img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://githut.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Définition


## Contexte
- Provisionner une EC2 avec une adresse IP publique

- L'IP publique doit être attachée à l'instance EC2.

- Créer un security group et ouvrir les ports 80 et 443.

- Attacher les SG à l'EC2.

- Le type d'instance EC2 doit être paramétrable. Le type par défaut sera un t2.nano, et cette variable sera surchargée par un t3.medium.

- L'image à utiliser est la plus récente de Debian présente dans le catalogue d'images d'Amazon.

- La clé à utiliser sera devops-<tag_au_choix>.

- Paramétrer le tag associé à l'instance pour qu'il puisse être surchargé par la suite.

- Une fois l'infrastructure provisionnée, supprimer les ressources.

## Prérequis 
[Installer terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab1-intall-terraform)
[créer un compte aws](https://gitlab.com/CarlinFongang-Labs/Terraform/lab0-sigup-aws)

## Ressources
[Terraform - aws security group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group)

## Définition du fichier de variables
L'on va créer un fichier qui va contenir toutes les variable **"variables.tf"**

````bash
variable "instance_type" {
  type        = string
  description = "set aws instance type"
  default     = "t2.nano"
}

variable "aws_common_tag" {
  type        = map
  description = "Common tag for resources"
  default     = {
    values = "ec2-aDigital"
  }
}
````
Définition des variables "instance_type" et "aws_common_tag" avec leurs types, descriptions et valeurs par défaut

## Définition du fichier de surchage des variables
**"terraform.tfvars"**

````bash
instance_type = "t3.medium"
aws_common_tag = {
  Name = "ec2-aCloud.Digital"
}
````
Définit le type d'instance et les tags communs pour une instance EC2 sur AWS

## définition de la ressource "ami" dans le fichier ec2.tf

data "aws_ami" "app_ami" {
  most_recent = true
  owners = ["amazon"]

  filter {
    name = "name"
    values = ["Cloud9Ubuntu-2023-07-14T10-48*"]
  }
}

Une fois l'ami variablisé, on peut l'utiliser dans la déclaration de la ressource **"aws_instance"**
Le type d'instance été variabilisé : **"instance_type" = var.instance_type**
Le tag a été variabilisé : **"tags" = var.aws_common_tag**
Ainsi que le security group : **"security_groups = ["${aws_security_group.allow_http_https.name}"]"**

````bash
resource "aws_instance" "ec2-tf" {
  ami           = data.aws_ami.app_ami.id
  instance_type = var.instance_type
  key_name      = "devops-aCloud.Digital"
  tags          = var.aws_common_tag
  security_groups = ["${aws_security_group.allow_http_https.name}"]
}
````


## définition du security group
````bash
resource "aws_security_group" "allow_http_https" {
  name        = "aCloud.Digital-sg"
  description = "Allow HTTP and HTTPS inbound traffic and all outbound traffic"

  ingress {
    description = "HTTPS from VPC"
    from_port         = 443
    protocol       = "tcp"
    to_port           = 443
    cidr_blocks      = ["0.0.0.0/0"]
  }
    ingress {
    description = "HTTP from VPC"
    from_port         = 80
    protocol       = "tcp"
    to_port           = 80
    cidr_blocks      = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

````

## Définition de l'IP public
[doc epi ressource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip)

````bash
resource "aws_eip" "lb" {
  instance = aws_instance.ec2-tf.id
  vpc = true
}
````

## Exécution et provisionnement avec terraform
1. terraform init
initialisation des plugin du provider
>![alt text](image.png)

2. terraform plan 
>![alt text](img/image-1.png)

3. terraform apply
>![alt text](img/image-2.png)

4. Vue du provisionnement depuis la console aws
>![alt text](img/image-3.png)
*Instance provisionné*

>![alt text](img/image-4.png)
*Security groups provisionné*

## Suppression de l'infrastucture : terraform destroy
>![alt text](img/image-5.png)
*Suppression des ressources*