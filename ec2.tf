// Ce code Terraform configure le fournisseur AWS avec les informations d'identification nécessaires pour accéder à la région us-east-1.
provider "aws" {
  region     = "us-east-1"
  access_key = "AKIAWDTBRRZMRAA4DC7H"
  secret_key = "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"

}

data "aws_ami" "app_ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["Cloud9Ubuntu-2023-07-14T10-48*"]
  }
}

resource "aws_instance" "ec2-tf" {
  ami             = data.aws_ami.app_ami.id
  instance_type   = var.instance_type
  key_name        = "devops-aCloud.Digital"
  tags            = var.aws_common_tag
  security_groups = ["${aws_security_group.allow_http_https.name}"]
}

resource "aws_security_group" "allow_http_https" {
  name        = "aCloud.Digital-sg"
  description = "Allow HTTP and HTTPS inbound traffic and all outbound traffic"

  ingress {
    description = "HTTPS from VPC"
    from_port   = 443
    protocol    = "tcp"
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "HTTP from VPC"
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_eip" "lb" {
  instance = aws_instance.ec2-tf.id
  domain   = "vpc"
}